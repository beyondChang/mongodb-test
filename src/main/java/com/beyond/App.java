package com.beyond;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.bson.Document;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

public class App {
	public static void main(String[] args) throws ParseException {

		MongoDatabase mongoDatabase = MongoDBUtil.getConnect("182.92.207.178", "test");
		// 获取集合
		MongoCollection<Document> collection = mongoDatabase.getCollection("time");

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

		int state = 1;
		Date lasttime = sdf.parse("2020-07-09 14:00:00.000");
		Date endtime = sdf.parse("2020-07-10 14:00:00.000");
		Date 为1的时间点 = null;
		Date 为0的时间点 = null;
		Long secs = (long) 0;
		Long secsall = (long) 0;
		long startTime=System.currentTimeMillis();
		while (lasttime.getTime() < endtime.getTime()) {
			BasicDBList condList = new BasicDBList();
			BasicDBObject cond1 = new BasicDBObject();
			cond1.append("t", new BasicDBObject("$gte", lasttime));
			BasicDBObject cond2 = new BasicDBObject();
			cond2.append("v", new BasicDBObject("$eq", state));
			condList.add(cond1);
			condList.add(cond2);
			BasicDBObject cond = new BasicDBObject();
			cond.put("$and", condList);
			Document doc = collection.find(cond).sort(Filters.eq("t", 1)).first();
			Date date = doc.getDate("t");
			lasttime = sdf.parse(sdf.format(date));
//			System.err.println(sdf.format(date));
			if (state == 1) {
				为1的时间点 = lasttime;
				state = 0;
				if (为0的时间点 != null) {
					List<Date> list = new ArrayList<Date>();
					list.add(为1的时间点);
					list.add(为0的时间点);
					// 聚合查询
					Document sub_project = new Document();
					sub_project.put("timesub", new Document("$subtract", list));

					Document project = new Document("$project", sub_project);
					Document limit = new Document("$limit", 1);

					List<Document> aggregateList = new ArrayList<Document>();
					aggregateList.add(project);
					aggregateList.add(limit);

					AggregateIterable<Document> iterable = collection.aggregate(aggregateList);
					doc = iterable.first();
					secs = doc.getLong("timesub");
					secsall += secs;
					
				}
			} else {
				为0的时间点 = lasttime;
				state = 1;
			}
		}
		long endTime=System.currentTimeMillis(); //获取结束时间
		System.out.println("程序运行时间： "+(endTime-startTime)+"ms");
		double 总生产时间 = 3600*24-secsall/1000;
		double 设备稼动率 = 总生产时间/(3600*24)*100;
		System.err.println("总共在产时间为:"+总生产时间+"秒,当天该设备稼动率为:"+String.format("%.2f", 设备稼动率)+"%");

	}
}

//

//FindIterable<Document> iterable = collection.find().sort(Filters.eq("t", 1));
//MongoCursor<Document> cursor = iterable.iterator();
//while (cursor.hasNext()) {
//	Document doc = cursor.next();
//	Document doc = collection.find().first();
//	Date date = doc.getDate("date");
//	SimpleDateFormat formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
//	formattedDate.setTimeZone(TimeZone.getTimeZone("UTC"));
//	System.out.println(formattedDate.format(date));
//	System.out.println(doc);
//}